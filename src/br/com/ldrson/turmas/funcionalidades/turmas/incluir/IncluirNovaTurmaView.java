package br.com.ldrson.turmas.funcionalidades.turmas.incluir;

/**
 * Created by leanderson on 24/06/16.
 */
public class IncluirNovaTurmaView {


    private IncluirNovaTurmaController mController;

    public IncluirNovaTurmaView(IncluirNovaTurmaController controller) {
        mController = controller;
    }

    public void exibirCabecalho(){
        System.out.println("|================================================|");
        System.out.println("|                NOVA TURMA                      |");
        System.out.println("|================================================|");
        System.out.println("|                                                |");
    }

    public void exibirSolicitacaoDoCodigo(){
        System.out.println("|  Digite o código : ");
    }

    public void exibirSolicitacaoDoNome(){
        System.out.println("|  Digite o nome : ");
    }

    public void exibirSolicitacaoDoCurso(){
        System.out.println("|  Digite o curso : ");
    }

    public void exibirSolicitacaoDoAno(){
        System.out.println("|  Digite o ano  : ");
    }


    public void exibirMensagemDeTurmaCadastradaComSucesso() {
        System.out.println("|================================================|");
        System.out.println("|  Nova Turma cadastrada com sucesso!            |");
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeParaProsseguir() {
        System.out.println("|  Tecle ENTER para continuar                    |");
        System.out.println("|================================================|");
    }
}
