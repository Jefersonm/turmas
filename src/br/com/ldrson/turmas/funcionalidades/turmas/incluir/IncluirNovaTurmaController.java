package br.com.ldrson.turmas.funcionalidades.turmas.incluir;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class IncluirNovaTurmaController {

    private IncluirNovaTurmaView mView;
    private Turma mTurma;
    private Scanner mScanner;
    private TurmaDAO mDao;

    public IncluirNovaTurmaController(){
        mView = new IncluirNovaTurmaView(this);
        mScanner = new Scanner(System.in);
        mTurma = new Turma();
        mDao = new TurmaDAO();
    }

    public void executar(){

        mView.exibirCabecalho();

        mView.exibirSolicitacaoDoCodigo();
        mTurma.setCodigo(mScanner.nextLine());

        mView.exibirSolicitacaoDoNome();
        mTurma.setNome(mScanner.nextLine());

        mView.exibirSolicitacaoDoCurso();
        mTurma.setCurso(mScanner.nextLine());

        mView.exibirSolicitacaoDoAno();
        mTurma.setAno(mScanner.nextInt());
        mScanner.nextLine();

        salvarNovaTurma();
        mView.exibirMensagemDeTurmaCadastradaComSucesso();
        mView.exibirMensagemDeParaProsseguir();

        mScanner.nextLine();

    }

    public void salvarNovaTurma(){
        mDao.salvar(mTurma);
    }

}
