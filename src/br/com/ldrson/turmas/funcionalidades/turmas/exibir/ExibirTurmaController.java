package br.com.ldrson.turmas.funcionalidades.turmas.exibir;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class ExibirTurmaController {

    private Turma mTurma;
    private TurmaDAO mDao;
    private ExibirTurmaView mView;
    private Scanner mScanner;

    public ExibirTurmaController(){
        mDao = new TurmaDAO();
        mView = new ExibirTurmaView();
        mScanner = new Scanner(System.in);
    }

    public void executar(){
        mView.solicitarCodigoDaTurmaParaExibir();
        String codigo = mScanner.nextLine();

        mTurma = mDao.obter(codigo);

        if(mTurma == null){
            mView.exibirMensagemTurmaComCodigoInfomrmadoNaoExiste(codigo);
            mView.exibirMensagemDeParaProsseguir();
            mScanner.nextLine();
        }else{
            mView.exibirTurma(mTurma);
            mView.exibirMensagemDeParaProsseguir();
            mScanner.nextLine();
        }
    }



}
