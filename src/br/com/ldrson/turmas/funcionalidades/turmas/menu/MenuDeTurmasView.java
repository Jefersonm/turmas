package br.com.ldrson.turmas.funcionalidades.turmas.menu;

/**
 * Created by leanderson on 24/06/16.
 */
public class MenuDeTurmasView {

    public void exibirOpcoes(){
        System.out.println("|================================================|");
        System.out.println("|              MENU DE TURMAS                    |");
        System.out.println("|================================================|");
        System.out.println("|                                                |");
        System.out.println("|    L - Listar todas as turmas                  |");
        System.out.println("|    I - Incluir uma nova turma                  |");
        System.out.println("|    C - Consultar uma Turma                     |");
        System.out.println("|    E - Editar uma Turma                        |");
        System.out.println("|    D - Deletar uma Turma                       |");
        System.out.println("|                                                |");
        System.out.println("|    X - Para Sair                               |");
        System.out.println("|                                                |");
        System.out.println("|================================================|");
        System.out.println("| Digite a opção desejada ::                     |");
        System.out.println("|================================================|");
    }

}
