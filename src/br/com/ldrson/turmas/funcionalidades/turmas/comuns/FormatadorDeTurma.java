package br.com.ldrson.turmas.funcionalidades.turmas.comuns;

import br.com.ldrson.turmas.comuns.dominio.Turma;

/**
 * Created by leanderson on 24/06/16.
 */
public class FormatadorDeTurma {

    public final static String mDelimitador = "&";

    public String formatar(Turma turma){
        return turma.getCodigo()+ mDelimitador +turma.getCurso()+ mDelimitador +turma.getNome()+mDelimitador+turma.getAno();
    }

    public Turma obterDaString(String linha){
        String campos[] = linha.split(mDelimitador);

        Turma turma = new Turma();
        turma.setCodigo(campos[0]);
        turma.setCurso(campos[1]);
        turma.setNome(campos[2]);
        turma.setAno(Integer.parseInt(campos[3]));

        return turma;
    }

}
