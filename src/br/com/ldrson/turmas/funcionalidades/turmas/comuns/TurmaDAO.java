package br.com.ldrson.turmas.funcionalidades.turmas.comuns;

import br.com.ldrson.turmas.arquivo.GerenciadorDeArquivo;
import br.com.ldrson.turmas.comuns.dominio.Turma;

import java.util.ArrayList;

/**
 * Created by leanderson on 24/06/16.
 */
public class TurmaDAO {

    public final static String mNomeDoArquivo = "turmas.dat";

    private GerenciadorDeArquivo mGerenciadorDeArquivo;
    private FormatadorDeTurma mFormatador;

    public TurmaDAO(){
        mGerenciadorDeArquivo = new GerenciadorDeArquivo();
        mGerenciadorDeArquivo.inicializarArquivo(mNomeDoArquivo);
        mFormatador = new FormatadorDeTurma();
    }

    private void inserir(Turma turma){
        mGerenciadorDeArquivo.adicionarLinhaNoFinalDoArquivo(mNomeDoArquivo,mFormatador.formatar(turma));
    }

    public void salvar(Turma turma){
        if(obter(turma.getCodigo()) != null){
            atualizar(turma);
        }else{
            inserir(turma);
        }
    }

    private void atualizar(Turma turma){
        mGerenciadorDeArquivo.atualizarLinha(mNomeDoArquivo,mFormatador.formatar(turma),obterPosicaoDoRegistroNoArquivo(turma.getCodigo()));
    }

    public void deletar(Turma turma) {
        mGerenciadorDeArquivo.excluirLinha(mNomeDoArquivo,obterPosicaoDoRegistroNoArquivo(turma.getCodigo()));
    }

    public Turma obter(String codigo){

        Turma turma = null;

        for(Turma t : obterTodos()){
            if(t.getCodigo().equals(codigo)){
                turma = t;
                break;
            }
        }

        return turma;
    }

    public int obterPosicaoDoRegistroNoArquivo(String codigo){

        int posicaoDoRegistro = -1;

        for(Turma t : obterTodos()){
            posicaoDoRegistro++;
            if(t.getCodigo().equals(codigo)){
                break;
            }
        }

        return posicaoDoRegistro;
    }

    public ArrayList<Turma> obterTodos(){
        ArrayList<Turma> turmas = new ArrayList<>();

        ArrayList<String> linhas = mGerenciadorDeArquivo.lerArquivoPorLinha(mNomeDoArquivo);

        for(String linha : linhas){
            turmas.add(mFormatador.obterDaString(linha));
        }

        return turmas;
    }

}
